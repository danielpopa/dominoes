## Dominoes game

Domino game with 2-4 players

## Installation

$ composer install

## Usage

$ php artisan domino:play
