<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Player;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class PlayDomino extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domino:play';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Play Dominoes game (2-4 players)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function handle(): string
    {
        // Get number of players
        $players = $this->askValid(
            "Chose number of players between 2 and 4",
            'noPlayer',
            ['required', 'numeric', 'between:2,4']
        );

        // Collect players name's
        $collectionPlayers = new Collection();
        foreach (range(1, $players) as $id) {
            $name = $this->askValid(
                "Player $id: What's your name?",
                'name',
                ['required', 'string']
            );
            $collectionPlayers->add(new Player($id, $name));
        }

        $this->line('Starting the game! Shuffling tiles! ...');
        $this->line('Each player has received 7 tiles.');
        // Add players to game and start the flow
        $game = new Game($collectionPlayers);
        $player = $game->start();

//        $this->line("The board tiles: " . $game->showBoardTiles());
//
//        foreach ($game->getPlayers() as $player) {
//            var_dump($player->getTiles());exit;
//        }

        do {
            $this->line(">> (Player " . $player->getId() . ") " . $player->getName() . "'s turn <<");
            $this->line(">> Your tiles: " . collect($player->getTiles())->implode(' ') . " <<");
            $tileSelected = $this->askValid(
                "Chose a tile in format x:x",
                'tileSelected',
                ['required', 'string', 'regex:/^\d{1}:\d{1}$/']
            );
            var_dump($tileSelected);exit;
            $game->turn($player);
        } while ($game->getWinner() != 0);

        return $players;
    }

    /**
     * @param $question
     * @param $field
     * @param $rules
     * @return string
     */
    protected function askValid($question, $field, $rules): string
    {
        $value = $this->ask($question);

        if($message = $this->validateInput($rules, $field, $value)) {
            $this->error($message);

            return $this->askValid($question, $field, $rules);
        }

        return $value;
    }


    protected function validateInput($rules, $fieldName, $value): ?string
    {
        $validator = Validator::make([
            $fieldName => $value
        ], [
            $fieldName => $rules
        ]);

        return $validator->fails()
            ? $validator->errors()->first($fieldName)
            : null;
    }
}
