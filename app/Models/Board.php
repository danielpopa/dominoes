<?php

namespace App\Models;

use Facade\FlareClient\Http\Exceptions\MissingParameter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    use HasFactory;

    /**
     * @var array $deck
     */
    private array $deck;

    /**
     * @var array $line
     */
    private array $line;

    /**
     * @param array $deck
     * @param array $attributes
     */
    public function __construct(array $deck, array $attributes = [])
    {
        parent::__construct($attributes);
        $this->deck = $deck;
    }

    /**
     * @return array
     */
    public function getDeckTiles(): array
    {
        return $this->deck;
    }

    /**
     * @param array $deck
     */
    public function setDeckTiles(array $deck): void
    {
        $this->deck = $deck;
    }

    /**
     * @throws MissingParameter
     */
    public function pullDeckTile(string $tile)
    {
        if (!in_array($tile, $this->deck)) {
            throw new MissingParameter('TILE_NOT_IN_BOARD');
        }

        $this->deck = array_filter($this->deck, fn ($m) => $m != $tile);
    }

    /**
     * @return array
     */
    public function getLine(): array
    {
        return $this->line;
    }

    /**
     * @param array $line
     */
    public function setLine(array $line): void
    {
        $this->line = $line;
    }
}
