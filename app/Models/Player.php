<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    /**
     * @var int $id
     */
    private int $id;

    /**
     * @var string $name
     */
    private string $name;

    /**
     * @var array $tiles
     */
    public array $tiles;

    /**
     * @var int $turn
     */
    public static int $turn = 0;

    public function __construct(int $id, string $name, array $attributes = [])
    {
        parent::__construct($attributes);
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getTiles(): array
    {
        return $this->tiles;
    }

    /**
     * @param array $tiles
     */
    public function setTiles(array $tiles): void
    {
        $this->tiles = $tiles;
    }
}
