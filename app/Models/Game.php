<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Game extends Model
{
    use HasFactory;

    /**
     * @var Collection $players
     */
    private Collection $players;

    /**
     * @var Board $boardTiles
     */
    private Board $boardTiles;


    /**
     * @var int
     */
    private int $winner = 0;

    public function __construct(Collection $players, array $attributes = [])
    {
        parent::__construct($attributes);
        $this->players = $players;
    }

    /**
     * @return Player
     */
    public function start(): Player
    {
        // init Board with shuffled tiles
        $tiles = ['0:0', '0:1', '0:2', '0:3', '0:4', '0:5', '0:6', '1:1', '1:2', '1:3', '1:4', '1:5', '1:6',
            '2:2', '2:3', '2:4', '2:5', '2:6', '3:3', '3:4', '3:5', '3:6', '4:4', '4:5', '4:6', '5:5', '5:6', '6:6'];
        $this->boardTiles = new Board($this->shuffle_assoc($tiles));
        $boardTiles = $this->boardTiles->getDeckTiles();

        $startPlayer = null;
        $startValue = null;
        // assign each player 7 tiles
        foreach ($this->players as $player) {
            $player->tiles = array_splice($boardTiles, 0, 7);
            foreach ($player->tiles as $tile) {
                $a = explode(':', $tile);
                if ($a[0] == $a[1] && $a[0] > $startValue) {
                    $startValue = $a[0];
                    $startPlayer = $player;
                }
            }
            $this->boardTiles->setDeckTiles($boardTiles);
        }

        return $startPlayer;
    }

    /**
     * @return Collection
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    /**
     * @return string
     */
    public function showBoardTiles(): string
    {
        return collect($this->boardTiles->getDeckTiles())->implode(' ');
    }

    /**
     * @param $list
     * @return array
     */
    protected function shuffle_assoc($list): array
    {
        if (!is_array($list)) { return $list; }
        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    /**
     * @param Player $player
     * @return bool
     */
    public function turn(Player $player): bool
    {
        if (count($this->boardTiles->getDeckTiles()) == 0) {
            $this->setWinner($player->getId());
            return false;
        }

        return true;
    }

    public function getWinner(): int
    {
        return $this->winner;
    }

    /**
     * @param int $winner
     */
    public function setWinner(int $winner): void
    {
        $this->winner = $winner;
    }
}
